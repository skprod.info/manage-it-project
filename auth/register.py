import tkinter as tk
from tkinter import messagebox
import sqlite3

def register(root, c, conn, refresh_users):
    def register_new_user(username_entry, password_entry):
        username = username_entry.get()
        password = password_entry.get()

        try:
            c.execute('INSERT INTO users (username, password) VALUES (?, ?)', (username, password))
            conn.commit()
            messagebox.showinfo("Success", "User registered.")
            register_frame.destroy()
            refresh_users()
        except sqlite3.IntegrityError:
            messagebox.showerror("Error", "User with this username already exists.")

    register_frame = tk.Frame(root, borderwidth=1, relief="solid")
    register_frame.place(relx=0.5, rely=0.5, anchor="center")

    inner_frame = tk.Frame(register_frame, padx=20, pady=10)
    inner_frame.pack()

    username_label = tk.Label(inner_frame, text="Username:")
    username_label.grid(row=0, column=0)
    username_entry = tk.Entry(inner_frame)
    username_entry.grid(row=0, column=1)

    password_label = tk.Label(inner_frame, text="Password:")
    password_label.grid(row=1, column=0)
    password_entry = tk.Entry(inner_frame, show="*")
    password_entry.grid(row=1, column=1)

    back_button = tk.Button(inner_frame, text="Back", command=root.show_login_form)
    back_button.grid(row=3, columnspan=2, pady=10)
    register_button = tk.Button(inner_frame, text="Register", command=lambda: register_new_user(username_entry, password_entry))
    register_button.grid(row=2, columnspan=2, pady=10)