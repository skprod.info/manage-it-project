import tkinter as tk
from tkinter import messagebox, simpledialog, Menu, ttk
import sqlite3
from datetime import datetime
import openpyxl
from tkinter.filedialog import asksaveasfilename
from tkinter import filedialog
import time
from translation_module.translations import TranslationManager
from tkinter import StringVar

class ToDoApp:
    def __init__(self, root):
        self.root = root
        self.language = "ru"
        self.root.title("ISTU PROJECT MANAGE")
        self.root.geometry("1220x800")
        self.translation_manager = TranslationManager()

        self.style = ttk.Style()
        self.style.configure('TButton', font=('Montserrat', 10), foreground='white', background='#4CAF50', padding=10)
        self.style.configure('TLabel', font=('Montserrat', 10))
        self.style.configure('TFrame', background='#f0f0f0')
        self.style.configure('Treeview', font=('Montserrat', 10), background='white', foreground='black', rowheight=30)

        self.style.configure('TFrame', background='#f0f0f0', relief='raised', borderwidth=2)

        self.style.configure('Treeview', font=('Montserrat', 10), background='white', foreground='black', rowheight=30)


        # Создаем базу данных для хранения задач и пользователей
        self.conn = sqlite3.connect('tasks.db')
        self.c = self.conn.cursor()

        # Проверяем, авторизован ли пользователь
        self.logged_in = False

        # Отображаем поля ввода для авторизации/регистрации
        self.show_login_form()

        # Создаем меню
        self.create_menu()

        self.img = tk.PhotoImage(file="images/logo.png")

        # Время онлайн
        self.time_label = tk.Label(self.root, font=("Arial", 14))
        self.time_label.pack()
        self.display_current_time()


    def create_menu(self):
        menu = Menu(self.root)
        self.root.config(menu=menu)

        # Программа
        self.program_menu = Menu(menu, tearoff=False)
        menu.add_cascade(label="Программа", menu=self.program_menu)
        self.program_menu.add_command(label="Настройки", command=self.settings)
        self.program_menu.add_command(label="Добавить пользователя", command=self.add_user)
        self.program_menu.add_command(label="Выгрузка отчета", command=self.create_report)
        self.program_menu.add_separator()
        self.program_menu.add_command(label="Выход", command=self.exit_program)

        # Сотрудники
        self.employees_menu = Menu(menu, tearoff=False)
        menu.add_cascade(label="Сотрудники", menu=self.employees_menu)
        self.employees_menu.add_command(label="Просмотреть список", command=self.show_employees)
        self.employees_menu.add_command(label="Удалить сотрудника", command=self.delete_employee)
        self.employees_menu.entryconfig(0, state="disabled")

        # Мои задачи
        menu.add_command(label="Мои задачи", command=self.show_my_tasks)

        # Архив
        self.archive_menu = Menu(menu, tearoff=False)
        menu.add_cascade(label="Архив", menu=self.archive_menu)
        self.archive_menu.add_command(label="Показать архив", command=self.show_archive_window)

        # Сохранение ссылок на элементы меню
        self.menu = menu


    def show_my_tasks(self):
        # Получаем текущего пользователя
        user_info = self.get_current_user_id()
        if user_info is not None:
            current_user_id = user_info

            # Получаем задачи, назначенные текущему пользователю
            assigned_tasks = self.c.execute('SELECT * FROM tasks WHERE performer_id=?', (current_user_id,)).fetchall()

            # Получаем отслеживаемые задачи текущего пользователя
            tracked_tasks = self.c.execute(
                'SELECT tasks.* FROM tasks JOIN tracking_tasks ON tasks.id = tracking_tasks.task_id WHERE tracking_tasks.user_id=?',
                (current_user_id,)).fetchall()

            # Объединяем задачи в один список
            all_tasks = assigned_tasks + tracked_tasks

            # Отображаем задачи в новом окне
            self.tasks_window = tk.Toplevel(self.root)
            self.tasks_window.title(self.translation_manager.get_translation("my_tasks"))
            self.tasks_window.geometry("1220x600")

            # Создаем таблицу для отображения задач
            self.task_list = ttk.Treeview(self.tasks_window, columns=("ID", "Task", "Status", "Date", "Urgency", "Performer"), show="headings")
            self.task_list.heading("ID", text=self.translation_manager.get_translation("ID"))
            self.task_list.heading("Task", text=self.translation_manager.get_translation("Task"))
            self.task_list.heading("Status", text=self.translation_manager.get_translation("Status"))
            self.task_list.heading("Date", text=self.translation_manager.get_translation("Date"))
            self.task_list.heading("Urgency", text=self.translation_manager.get_translation("Urgency"))
            self.task_list.heading("Performer", text=self.translation_manager.get_translation("Performer"))
            self.task_list.grid(row=0, column=0, columnspan=6, sticky="ew")

            # Заполняем таблицу данными о задачах
            for task in all_tasks:
                self.task_list.insert("", "end", values=task)
        else:
            messagebox.showerror(self.translation_manager.get_translation("Error"), self.translation_manager.get_translation("user_id_error"))

    def show_employees(self):
        employees_window = tk.Toplevel(self.root)
        employees_window.title("Сотрудники")
        employees_window.geometry("400x300")
        # Получаем список всех сотрудников из таблицы users
        employees = self.c.execute('SELECT * FROM users').fetchall()
        # Отображаем список сотрудников в новом окне
        for idx, employee in enumerate(employees, start=1):
            employee_label = tk.Label(employees_window, text=f"{idx}. {employee[1]}")
            employee_label.pack()

    def get_current_user_id(self):
        # Получаем имя пользователя из поля ввода
        username = self.username_entry.get()
        password = self.password_entry.get()

        # Получаем ID текущего пользователя по его имени
        self.c.execute('SELECT id FROM users WHERE username=? AND password=?', (username, password))
        user_id = self.c.fetchone()
        if user_id:
            return user_id[0]  # Возвращаем только ID пользователя
        else:
            return None

    def delete_employee(self):
        # Получаем роль текущего пользователя
        role = self.get_user_role()
        if self.is_admin(role):
            # Получаем ID выбранного сотрудника
            selected_employee_index = simpledialog.askinteger("Удаление сотрудника",
                                                              "Введите номер сотрудника для удаления:",
                                                              parent=self.root)
            if selected_employee_index is not None and selected_employee_index > 0:
                # Получаем ID сотрудника по его номеру
                self.c.execute('SELECT id FROM users ORDER BY id LIMIT 1 OFFSET ?', (selected_employee_index - 1,))
                employee_id = self.c.fetchone()
                if employee_id:
                    # Удаляем сотрудника из базы данных
                    self.c.execute('DELETE FROM users WHERE id=?', (employee_id[0],))
                    self.conn.commit()
                    messagebox.showinfo("Успешно", "Сотрудник удален.")
                    self.refresh_users()
                    self.show_employees()
                else:
                    messagebox.showerror("Ошибка", "Неверный номер сотрудника.")
            elif selected_employee_index == 0:
                messagebox.showerror("Ошибка", "Неверный номер сотрудника.")
        else:
            messagebox.showerror("Ошибка", "Недостаточно прав для удаления сотрудника.")

    def get_user_role(self):
        # Получаем роль текущего пользователя
        username = self.username_entry.get()  # Предполагается, что имя пользователя уже введено
        self.c.execute('SELECT role FROM users WHERE username=?', (username,))
        role = self.c.fetchone()
        if role:
            return role[0]
        else:
            return None

    def is_admin(self, role):
        # Проверяем, является ли указанная роль администратором
        username = self.username_entry.get()  # Предполагается, что имя пользователя уже введено
        self.c.execute('SELECT role FROM users WHERE username=?', (username,))
        role = self.c.fetchone()
        if role:
            return role[0]
        else:
            return None
        # return role == "admin"

    def display_current_time(self):
        current_time = datetime.now().strftime("%H:%M:%S")
        self.time_label.config(text=current_time)
        self.root.after(1000, self.display_current_time)

    def show_login_form(self):
        login_frame = tk.Frame(self.root, borderwidth=1, relief="solid")  # Добавляем рамку
        login_frame.place(relx=0.5, rely=0.5, anchor="center")  # Размещаем по центру

        # Добавляем внутренний фрейм для создания отступов
        inner_frame = tk.Frame(login_frame, padx=20, pady=10)
        inner_frame.pack()

        self.username_label = tk.Label(inner_frame, text="Имя пользователя:")
        self.username_label.grid(row=0, column=0)
        self.username_entry = tk.Entry(inner_frame)
        self.username_entry.grid(row=0, column=1)

        self.password_label = tk.Label(inner_frame, text="Пароль:")
        self.password_label.grid(row=1, column=0)
        self.password_entry = tk.Entry(inner_frame, show="*")
        self.password_entry.grid(row=1, column=1)

        self.login_button = tk.Button(inner_frame, text="Войти", command=self.login)
        self.login_button.grid(row=2, columnspan=2, pady=10)

        self.register_button = tk.Button(inner_frame, text="Регистрация", command=self.register)
        self.register_button.grid(row=3, columnspan=2)

    def login(self):
        username = self.username_entry.get()
        password = self.password_entry.get()

        self.c.execute('SELECT * FROM users WHERE username=? AND password=?', (username, password))
        user = self.c.fetchone()
        if user:
            self.logged_in = True
            self.show_main_text()
            # Передача имени пользователя методу show_todo_app()
            self.show_todo_app(username)
            self.employees_menu.entryconfig(0, state="normal")
            self.update_menu_text()  # Обновляем текстовые метки элементов меню
        else:
            messagebox.showerror("Ошибка", "Неправильное имя пользователя или пароль.")

        if self.logged_in and self.is_admin(self.logged_in):
            self.menu.entryconfig(2, state="normal")  # Индекс элемента "Выгрузка отчета" в меню
            self.menu.entryconfig(1, state="normal")  # Индекс элемента "Добавить пользователя" в меню
        self.update_menu_text()

    def show_main_text(self):
        # Создание фрейма для размещения виджетов
        main_frame = tk.Frame(self.root)
        main_frame.pack(expand=True, fill="both")

        # Текстовая метка
        main_text = tk.Label(main_frame, text="ISTU PROJECT MANAGE", font=("Montserrat", 24, "bold"))
        main_text.grid(row=0, column=0, sticky="ew")

        # Метка с изображением
        image_label = tk.Label(main_frame, image=self.img)
        image_label.grid(row=0, column=1, sticky="ew")



    def register(self):
        register_frame = tk.Frame(self.root, borderwidth=1, relief="solid")  # Добавляем рамку
        register_frame.place(relx=0.5, rely=0.5, anchor="center")  # Размещаем по центру

        # Добавляем внутренний фрейм для создания отступов
        inner_frame = tk.Frame(register_frame, padx=20, pady=10)
        inner_frame.pack()

        username_label = tk.Label(inner_frame, text="ФИО:")
        username_label.grid(row=0, column=0)
        username_entry = tk.Entry(inner_frame)
        username_entry.grid(row=0, column=1)

        password_label = tk.Label(inner_frame, text="Пароль:")
        password_label.grid(row=1, column=0)
        password_entry = tk.Entry(inner_frame, show="*")
        password_entry.grid(row=1, column=1)
        def register_new_user():
            username = username_entry.get()
            password = password_entry.get()

            try:
                self.c.execute('INSERT INTO users (username, password) VALUES (?, ?)', (username, password))
                self.conn.commit()
                messagebox.showinfo("Успешно", "Пользователь зарегистрирован.")
                register_frame.destroy()  # Закрыть окно регистрации
                self.refresh_users()  # Обновить список пользователей
            except sqlite3.IntegrityError:
                messagebox.showerror("Ошибка", "Пользователь с таким именем уже существует.")

        back_button = tk.Button(inner_frame, text="Назад", command=self.show_login_form)
        back_button.grid(row=3, columnspan=2, pady=10)
        register_button = tk.Button(inner_frame, text="Зарегистрировать", command=register_new_user)
        register_button.grid(row=2, columnspan=2, pady=10)

    # def refresh_users(self):
    #     self.performer_combobox["values"] = [row[0] for row in self.c.execute('SELECT username FROM personal')]
    def refresh_users(self):
        if hasattr(self, 'performer_combobox') and self.performer_combobox:
            self.performer_combobox["values"] = [row[0] for row in self.c.execute('SELECT username FROM users')]
        else:
            print("Комбобокс не существует или был удален.")

    def show_todo_app(self, user):
        todo_frame = tk.Frame(self.root)
        todo_frame.pack(expand=True, fill='both')

        # Удаляем элементы формы для авторизации/регистрации
        self.username_label.grid_forget()
        self.username_entry.grid_forget()
        self.password_label.grid_forget()
        self.password_entry.grid_forget()
        self.login_button.grid_forget()
        self.register_button.grid_forget()

        # Отобразим основное приложение
        self.task_list = ttk.Treeview(todo_frame, columns=("ID", "Task", "Status", "Date", "Urgency", "Performer"),
                                      show="headings")

        self.task_list.heading("ID", text="Номер")
        self.task_list.heading("Task", text="Задача")
        self.task_list.heading("Status", text="Статус")
        self.task_list.heading("Date", text="Дата")
        self.task_list.heading("Urgency", text="Срочность")
        self.task_list.heading("Performer", text="Исполнитель")
        self.task_list.grid(row=0, column=0, columnspan=6, sticky="ew")

        self.scrollbar = tk.Scrollbar(todo_frame, orient="vertical", command=self.task_list.yview)
        self.scrollbar.grid(row=0, column=6, sticky='nsew')
        self.task_list.configure(yscrollcommand=self.scrollbar.set)

        self.task_entry_label = tk.Label(todo_frame, text="Задача:")
        self.task_entry_label.grid(row=2, column=0, sticky="w", padx=5)
        self.task_entry = tk.Entry(todo_frame)
        self.task_entry.grid(row=2, column=1, columnspan=5, sticky="ew", padx=5)

        self.status_label = tk.Label(todo_frame, text="Статус:")
        self.status_label.grid(row=3, column=0, sticky="w", padx=5)
        self.status_combobox = ttk.Combobox(todo_frame,
                                            values=["backlog", "in progress", "testing", "waiting for answer", "done",
                                                    "canceled"])
        self.status_combobox.set("backlog")
        self.status_combobox.grid(row=3, column=1, sticky="ew", padx=5)

        self.date_label = tk.Label(todo_frame, text="Дата:")
        self.date_label.grid(row=4, column=0, sticky="w", padx=5)
        self.date_entry = tk.Entry(todo_frame)
        self.date_entry.grid(row=4, column=1, sticky="ew", padx=5)
        self.date_entry.insert(0, datetime.now().strftime("%Y-%m-%d %H:%M:%S"))

        self.urgency_label = tk.Label(todo_frame, text="Срочность:")
        self.urgency_label.grid(row=5, column=0, sticky="w", padx=5)
        self.urgency_combobox = ttk.Combobox(todo_frame, values=["нормальная", "высокая", "срочная"])
        self.urgency_combobox.set("нормальная")
        self.urgency_combobox.grid(row=5, column=1, sticky="ew", padx=5)

        self.performer_label = tk.Label(todo_frame, text="Исполнитель:")
        self.performer_label.grid(row=6, column=0, sticky="w", padx=5)
        self.performer_combobox = ttk.Combobox(todo_frame)  # Создаем performer_combobox здесь
        self.performer_combobox.grid(row=6, column=1, sticky="ew", padx=5)
        self.refresh_users()  # Обновить список пользователей

        self.add_button = tk.Button(todo_frame, text="Добавить задачу", command=self.add_task, width=15)
        self.add_button.grid(row=7, column=0, columnspan=6, pady=10, sticky="w")

        self.delete_button = tk.Button(todo_frame, text="Удалить задачу", command=self.delete_task, width=15)
        self.delete_button.grid(row=7, column=0, columnspan=6)

        self.complete_button = tk.Button(todo_frame, text="Завершить задачу", command=self.complete_task, width=15)
        self.complete_button.grid(row=7, column=0, columnspan=6, sticky="e")

        current_username = user

        # Вывод имени пользователя
        username_label = tk.Label(todo_frame, text="Пользователь: " + current_username)
        username_label.grid(row=16, column=0, sticky="w", padx=5, pady=5)

        # Кнопка выхода
        logout_button = tk.Button(todo_frame, text="Выйти", command=self.show_program)
        logout_button.grid(row=17, column=0, sticky="w", padx=5, pady=5)

        # Добавляем метку с информацией о разработчике внизу окна
        developer_label = tk.Label(todo_frame, text="Систему разработал Костяев Сергей (DevPrime) специально для курсового проекта ИжГТУ", font=("Arial", 8), fg="gray")
        developer_label.grid(row=18, column=0, columnspan=6, sticky="ew", pady=(100, 0))

        self.refresh_tasks()
        self.task_list.bind("<Double-1>", self.show_task_details)

        # def refresh_users(self):
        #     if hasattr(self, 'performer_combobox') and self.performer_combobox:
        #         self.performer_combobox["values"] = [row[0] for row in self.c.execute('SELECT username FROM personal')]

        #Очистка главного окна

    def show_archive_window(self):
        archive_window = tk.Toplevel(self.root)
        archive_window.title("Архив задач")
        archive_window.geometry("800x400")

        # Создаем таблицу для отображения архивных задач
        archive_table = ttk.Treeview(archive_window, columns=("ID", "Task", "Status", "Date", "Urgency", "Performer"),
                                     show="headings")
        archive_table.heading("ID", text="Номер")
        archive_table.heading("Task", text="Задача")
        archive_table.heading("Status", text="Статус")
        archive_table.heading("Date", text="Дата")
        archive_table.heading("Urgency", text="Срочность")
        archive_table.heading("Performer", text="Исполнитель")
        archive_table.pack(expand=True, fill='both')

        # Заполняем таблицу данными об удаленных задачах
        deleted_tasks = self.c.execute('SELECT * FROM deleted_tasks').fetchall()
        for task in deleted_tasks:
            archive_table.insert("", "end", values=task)
    def show_program(self):
        # Скрыть все виджеты, кроме экрана авторизации и регистрации
        for widget in self.root.winfo_children():
            if not isinstance(widget, (tk.Menu, tk.Label)):
                widget.destroy()

        # Отображение экрана авторизации и регистрации
        self.show_login_form()


        # for widget in self.root.winfo_children():
        #     if isinstance(widget, tk.Menu):  # Проверяем, является ли виджет верхним меню
        #         continue  # Пропускаем верхнее меню
        #     widget.destroy()
        # # Отображение верхнего меню с пунктами "Время" и "Авторизация"
        # self.create_menu()
        # # Очистка или скрытие метки времени, если она существует
        # if hasattr(self, 'time_label') and self.time_label:
        #     self.time_label.destroy()  # Уничтожаем метку времени
        # # Создание и сохранение метки времени
        # self.time_label = tk.Label(self.root, text="", font=("Arial", 10))
        # self.time_label.pack()
        # # Отображение экрана авторизации
        # self.show_login_form()

    def show_task_details(self, event):
        self.c.execute('SELECT id FROM users')
        performer_ids = [row[0] for row in self.c.fetchall()]
        selected_item = self.task_list.selection()
        if selected_item:
            task_id = self.task_list.item(selected_item, "values")[0]
            task_details = self.c.execute('SELECT * FROM tasks WHERE id=?', (task_id,)).fetchone()
            if task_details:
                current_user_id = self.get_current_user_id()
                if current_user_id:
                    print(current_user_id)# Проверяем, успешно ли получен ID текущего пользователя
                    performer_id = task_details[8]
                    print(performer_id)
                    if current_user_id == performer_id:  # Проверяем, соответствует ли текущий пользователь исполнителю задачи
                        detail_window = tk.Toplevel(self.root)
                        detail_window.title("Подробности о задаче")
                        detail_window.geometry(
                            "600x500")  # Увеличил высоту для добавления функциональности прикрепления файла

                        task_label = tk.Label(detail_window, text="Задача:")
                        task_label.pack()
                        task_entry = tk.Entry(detail_window, width=50)
                        task_entry.pack()
                        task_entry.insert(0, task_details[1])

                        status_label = tk.Label(detail_window, text="Статус:")
                        status_label.pack()
                        status_combobox = ttk.Combobox(detail_window,
                                                       values=["backlog", "in progress", "testing", "waiting for answer",
                                                               "done", "canceled"])
                        status_combobox.pack()
                        status_combobox.set(task_details[2])

                        date_label = tk.Label(detail_window, text="Дата:")
                        date_label.pack()
                        date_entry = tk.Entry(detail_window)
                        date_entry.pack()
                        date_entry.insert(0, task_details[3])

                        urgency_label = tk.Label(detail_window, text="Срочность:")
                        urgency_label.pack()
                        urgency_combobox = ttk.Combobox(detail_window, values=["нормальная", "высокая", "срочная"])
                        urgency_combobox.pack()
                        urgency_combobox.set(task_details[4])

                        # Получение списка исполнителей из таблицы users
                        performers = [row[0] for row in self.c.execute('SELECT username FROM users')]
                        performer_label = tk.Label(detail_window, text="Исполнитель:")
                        performer_label.pack()
                        performer_combobox = ttk.Combobox(detail_window, values=performers)
                        performer_combobox.pack()
                        performer_combobox.set(task_details[5])

                        comments_label = tk.Label(detail_window, text="Комментарии:")
                        comments_label.pack()
                        comments_text = tk.Text(detail_window, height=5, width=50)
                        comments_text.pack()

                        if task_details[6]:  # Проверяем наличие комментариев
                            comments_text.insert("1.0", task_details[6])  # Отображение комментариев

                        # Отображение информации о пользователе, оставившем комментарий
                        comment_author_entry = tk.Entry(detail_window, width=50)  # Инициализация здесь
                        if task_details[7]:
                            comment_author_label = tk.Label(detail_window, text="Автор комментария:")
                            comment_author_label.pack()
                            comment_author_entry.pack()
                            comment_author_entry.insert(0, task_details[7])

                        # Добавление функциональности прикрепления файла
                        file_label = tk.Label(detail_window, text="Прикрепить файл:")
                        file_label.pack()
                        file_entry = tk.Entry(detail_window, width=50)
                        file_entry.pack()

                        def attach_file():
                            file_path = filedialog.askopenfilename()
                            file_entry.insert(tk.END, file_path)

                        attach_button = tk.Button(detail_window, text="Выбрать файл", command=attach_file)
                        attach_button.pack()

                        task_history = self.c.execute(
                            'SELECT status, performer, time_changed FROM task_history WHERE task_id=?', (task_id,)).fetchall()

                        # Отображение истории задачи
                        history_label = tk.Label(detail_window, text="История задачи:")
                        history_label.pack()
                        for entry in task_history:
                            history_entry_label = tk.Label(detail_window,
                                                           text=f"Статус: {entry[0]}, Исполнитель: {entry[1]}, Время изменения: {entry[2]}")
                            history_entry_label.pack()

                        # Создаем кнопку "Отслеживать задачу" (если текущий пользователь не является исполнителем)
                        # Получаем ID текущего пользователя
                        current_user_id = self.get_current_user_id()

                        # Получаем ID исполнителя задачи
                        performer_id = task_details[5]

                        # Проверяем, отслеживает ли уже текущий пользователь эту задачу
                        is_tracked = self.c.execute(
                            'SELECT COUNT(*) FROM tracking_tasks WHERE task_id=? AND user_id=?',
                            (task_id, current_user_id)
                        ).fetchone()[0] > 0

                        # Если текущий пользователь не является исполнителем и задача не отслеживается, отображаем кнопку "Отслеживать задачу"
                        if current_user_id != performer_id and not is_tracked:
                            track_button = tk.Button(detail_window, text="Отслеживать задачу",
                                                     command=lambda: self.track_task(task_id, detail_window))
                            track_button.pack()
                        # Если задача уже отслеживается текущим пользователем, отображаем кнопку "Прекратить отслеживать"
                        elif is_tracked:
                            stop_tracking_button = tk.Button(detail_window, text="Прекратить отслеживать",
                                                             command=lambda: self.stop_tracking_task(task_id, detail_window))
                            stop_tracking_button.pack()

                        def save_changes(comment_author_entry):
                            new_task = task_entry.get()
                            new_status = status_combobox.get()
                            new_date = date_entry.get()
                            new_urgency = urgency_combobox.get()
                            performer_index = performer_combobox.current()
                            new_performer = performers[performer_index]
                            new_performer_id = performer_ids[performer_index]
                            new_comments = comments_text.get("1.0", tk.END)
                            author = comment_author_entry.get() if comment_author_entry else None
                            # Получаем путь к прикрепленному файлу
                            attached_file_path = file_entry.get()

                            # Вставляем информацию о задаче в таблицу tasks
                            self.c.execute(
                                'UPDATE tasks SET task=?, status=?, date=?, urgency=?, performer=?, comments=? WHERE id=?',
                                (new_task, new_status, new_date, new_urgency, new_performer, new_comments, task_id))
                            self.conn.commit()

                            # Добавляем задачу в архив, если статус "canceled"
                            if new_status == "canceled":
                                self.move_to_archive(task_id)

                            # Добавляем запись в историю задачи
                            self.c.execute('INSERT INTO task_history (task_id, status, performer) VALUES (?, ?, ?)',
                                           (task_id, new_status, new_performer))
                            self.conn.commit()

                            messagebox.showinfo("Успешно", "Изменения сохранены.")
                            self.refresh_tasks()
                            detail_window.destroy()

                        # Создание кнопки "Сохранить изменения" за пределами функции save_changes
                        save_button = tk.Button(detail_window, text="Сохранить изменения",
                                                command=lambda: save_changes(comment_author_entry))
                        save_button.pack()

                        detail_window.update_idletasks()  # Пересчитываем размеры окна
                        detail_window.geometry(f"{detail_window.winfo_reqwidth()}x{detail_window.winfo_reqheight()}")
                    else:
                        # Запрещаем редактирование задачи
                        messagebox.showerror("Ошибка", "Вы не можете редактировать эту задачу.")
                else:
                    messagebox.showerror("Ошибка", "Не удалось определить текущего пользователя.")

    def stop_tracking_task(self, task_id, detail_window):
        try:
            self.c.execute('DELETE FROM tracking_tasks WHERE task_id=?', (task_id,))
            self.conn.commit()
            messagebox.showinfo("Успешно", "Отслеживание задачи прекращено.")
            detail_window.destroy()  # Закрываем окно после прекращения отслеживания
            self.refresh_tasks()
        except Exception as e:
            messagebox.showerror("Ошибка", f"Ошибка при прекращении отслеживания задачи: {e}")

    def track_task(self, task_id, detail_window):
        try:
            # Получаем ID текущего пользователя
            current_user_id = self.get_current_user_id()

            # Добавляем запись в базу данных отслеживаемых задач
            self.c.execute('INSERT INTO tracking_tasks (task_id, user_id) VALUES (?, ?)', (task_id, current_user_id))
            self.conn.commit()

            # Показываем сообщение об успешном отслеживании задачи
            messagebox.showinfo("Успешно", "Задача добавлена в отслеживаемые.")

            # Закрываем окно с подробностями о задаче
            detail_window.destroy()
        except Exception as e:
            messagebox.showerror("Ошибка", f"Ошибка при отслеживании задачи: {e}")

    def create_report(self):
        workbook = openpyxl.Workbook()
        sheet = workbook.active
        sheet.title = "Задачи"

        # Заголовки столбцов
        headers = ["ID", "Задача", "Статус", "Дата", "Срочность", "Исполнитель", "Комментарии"]
        for col, header in enumerate(headers, 1):
            sheet.cell(row=1, column=col, value=header)

        # Заполнение данными
        tasks_data = self.c.execute('SELECT * FROM tasks').fetchall()
        for row_idx, task_row in enumerate(tasks_data, start=2):
            for col_idx, cell_value in enumerate(task_row, start=1):
                sheet.cell(row=row_idx, column=col_idx, value=cell_value)

        # Запрашиваем путь для сохранения файла
        file_path = asksaveasfilename(defaultextension=".xlsx", filetypes=[("Excel files", "*.xlsx")])
        if file_path:
            workbook.save(file_path)

    def refresh_tasks(self):
        self.task_list.delete(*self.task_list.get_children())
        for row in self.c.execute('SELECT * FROM tasks'):
            self.task_list.insert("", "end", values=row)

    def add_task(self):
        task = self.task_entry.get()
        status = self.status_combobox.get()
        date = self.date_entry.get()
        urgency = self.urgency_combobox.get()
        performer = self.performer_combobox.get()

        self.c.execute(
            'INSERT INTO tasks (task, status, date, urgency, performer) VALUES (?, ?, ?, ?, ?)',
            (task, status, date, urgency, performer))
        self.conn.commit()
        self.task_entry.delete(0, tk.END)
        self.status_combobox.set("")
        self.urgency_combobox.set("")
        self.performer_combobox.set("")
        time.sleep(0.1)
        self.refresh_tasks()

        task_id = self.c.lastrowid
        # Получаем текущую дату и время
        current_time = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        self.c.execute('INSERT INTO task_history (task_id, status, performer, time_changed) VALUES (?, ?, ?, ?)',
                       (task_id, status, performer, current_time))
        self.conn.commit()

    def delete_task(self):
        try:
            selected_item = self.task_list.selection()
            if selected_item:
                task_values = self.task_list.item(selected_item, "values")
                task_id = task_values[0]

                # Получаем информацию о задаче
                task_info = self.c.execute('SELECT id, task, status, date, urgency, performer FROM tasks WHERE id=?',
                                           (task_id,)).fetchone()

                # Если задача найдена, перемещаем её в архив и удаляем из текущих задач
                if task_info:
                    self.move_to_archive(task_id)

                    # Обновляем отображение задач
                    self.refresh_tasks()
            else:
                messagebox.showerror("Ошибка", "Выберите задачу для удаления.")
        except Exception as e:
            print("Error:", e)

    def move_to_archive(self, task_id):
        # Получаем информацию о задаче
        task_info = self.c.execute('SELECT task, status, date, urgency, performer, comments FROM tasks WHERE id=?',
                                   (task_id,)).fetchone()

        # Если задача найдена, перемещаем её в архив
        if task_info:
            # Получаем максимальное значение id из таблицы deleted_tasks
            max_deleted_task_id = self.c.execute('SELECT MAX(id) FROM deleted_tasks').fetchone()[0]
            if max_deleted_task_id is None:
                max_deleted_task_id = 0
            new_deleted_task_id = max_deleted_task_id + 1

            # Добавляем задачу в таблицу удаленных задач (архив)
            self.c.execute(
                'INSERT INTO deleted_tasks (id, task, status, date, urgency, performer, comments) VALUES (?, ?, ?, ?, ?, ?, ?)',
                (new_deleted_task_id,) + task_info)
            self.conn.commit()

            # Удаляем задачу из таблицы текущих задач
            self.c.execute('DELETE FROM tasks WHERE id=?', (task_id,))
            self.conn.commit()

    def complete_task(self):
        selected_item = self.task_list.selection()
        if selected_item:
            task_id = self.task_list.item(selected_item, "values")[0]
            self.c.execute('UPDATE tasks SET status="Completed" WHERE id=?', (task_id,))
            self.conn.commit()
            self.refresh_tasks()

    def settings(self):
        self.settings_window = tk.Toplevel(self.root)
        self.settings_window.title(self.translation_manager.get_translation("settings"))
        self.settings_window.geometry("300x200")

        self.lang_label = tk.Label(self.settings_window, text=self.translation_manager.get_translation("select_language"))
        self.lang_label.pack(pady=10)

        self.language_var = StringVar(value=self.translation_manager.current_language)

        self.lang_combobox = ttk.Combobox(self.settings_window, textvariable=self.language_var, values=["ru", "en"])
        self.lang_combobox.pack(pady=10)

        self.save_button = tk.Button(self.settings_window, text=self.translation_manager.get_translation("save"), command=self.save_settings)
        self.save_button.pack(pady=10)

    def update_menu_text(self):
        self.program_menu.entryconfig(0, label=self.translation_manager.get_translation("settings"))
        self.program_menu.entryconfig(1, label=self.translation_manager.get_translation("add_user"))
        self.program_menu.entryconfig(2, label=self.translation_manager.get_translation("export_report"))
        self.program_menu.entryconfig(4, label=self.translation_manager.get_translation("exit"))


    def update_interface_text(self):
        # Обновляем текст меню
        self.menu.entryconfig(1, label=self.translation_manager.get_translation("menu_program"))
        self.program_menu.entryconfig(0, label=self.translation_manager.get_translation("settings"))
        self.program_menu.entryconfig(1, label=self.translation_manager.get_translation("add_user"))
        self.program_menu.entryconfig(2, label=self.translation_manager.get_translation("create_report"))
        self.program_menu.entryconfig(4, label=self.translation_manager.get_translation("exit"))

        self.menu.entryconfig(2, label=self.translation_manager.get_translation("menu_employees"))
        self.employees_menu.entryconfig(0, label=self.translation_manager.get_translation("view_list"))
        self.employees_menu.entryconfig(1, label=self.translation_manager.get_translation("delete_employee"))

        self.menu.entryconfig(3, label=self.translation_manager.get_translation("menu_tasks"))

        self.menu.entryconfig(4, label=self.translation_manager.get_translation("archive"))
        self.archive_menu.entryconfig(0, label=self.translation_manager.get_translation("show_archive"))

        # Обновляем текст других элементов интерфейса
        if hasattr(self, 'settings_window') and self.settings_window.winfo_exists():
            self.lang_label.config(text=self.translation_manager.get_translation("select_language"))
            self.save_button.config(text=self.translation_manager.get_translation("save"))

        # Обновление заголовков в таблице задач, если окно задач открыто
        if hasattr(self, 'tasks_window') and self.tasks_window.winfo_exists():
            for col_id in ["ID", "Task", "Status", "Date", "Urgency", "Performer"]:
                self.task_list.heading(col_id, text=self.translation_manager.get_translation(col_id))

    def save_settings(self):
        selected_lang = self.language_var.get()
        self.translation_manager.set_language(selected_lang)
        self.update_interface_text()
        self.update_menu_text()
        self.update_menu_text()
        self.settings_window.destroy()

    def set_language(self, lang):
        if lang in self.translations:
            self.current_language = lang

    def get_translation(self, key):
        return self.translations[self.current_language].get(key, key)

    def add_user(self):
        user_role = self.get_user_role()
        if user_role == "admin":
            register_frame = tk.Toplevel(self.root)
            register_frame.title("Регистрация пользователя")

            username_label = tk.Label(register_frame, text="ФИО:")
            username_label.grid(row=0, column=0)
            username_entry = tk.Entry(register_frame)
            username_entry.grid(row=0, column=1)

            job_label = tk.Label(register_frame, text="Должность:")
            job_label.grid(row=1, column=0)
            job_entry = tk.Entry(register_frame)
            job_entry.grid(row=1, column=1)

            password_label = tk.Label(register_frame, text="Пароль:")
            password_label.grid(row=2, column=0)
            password_entry = tk.Entry(register_frame, show="*")
            password_entry.grid(row=2, column=1)

            def register_new_user():
                username = username_entry.get()
                password = password_entry.get()
                job = job_entry.get()

                try:
                    self.c.execute('INSERT INTO users (username, password, job) VALUES (?, ?, ?)',
                                   (username, password, job))
                    self.conn.commit()
                    messagebox.showinfo("Успешно", "Пользователь зарегистрирован.")
                    register_frame.destroy()
                    self.refresh_app()  # Обновляем программу
                except sqlite3.IntegrityError:
                    messagebox.showerror("Ошибка", "Пользователь с таким именем уже существует.")

            register_button = tk.Button(register_frame, text="Зарегистрировать", command=register_new_user)
            register_button.grid(row=3, columnspan=2, pady=10)

        else:
            messagebox.showerror("Ошибка", "Недостаточно прав для добавления нового пользователя.")

    def refresh_app(self):
        # Обновляем выпадающий список пользователей
        self.refresh_users()

    def exit_program(self):
        self.root.destroy()

root = tk.Tk()
app = ToDoApp(root)
root.mainloop()
