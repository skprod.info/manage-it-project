# Система управления IT проектами (Manage IT Project - Project for ISTU IIzhevsk)

## Введение (Introduction)

В рамках данной работы мы проведем анализ существующих подходов к управлению IT проектами, выявим их преимущества и недостатки, а также предложим собственное видение системы управления, учитывающее специфику проектов в области информационных технологий.

As part of this work, we will analyze existing approaches to IT project management, identify their advantages and disadvantages, and also offer our own vision of the management system, taking into account the specifics of projects in the field of information technology.

![Иллюстрация к проекту](OtherPicture/fon1.png)
![Иллюстрация к проекту](OtherPicture/fon2.png)

## Начало (Start)

- [ ] [Загрузите](https://gitlab.com/skprod.info/manage-it-project/-/archive/main/manage-it-project-main.zip) файлы ([Upload](https://gitlab.com/skprod.info/manage-it-project/-/archive/main/manage-it-project-main.zip) files)
- [ ] [Добавьте файлы с помощью командной строки](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) или отправьте существующий Git репозиторий с помощью следующей команды: ([Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:)

```
cd existing_repo
git remote add origin https://gitlab.com/skprod.info/manage-it-project.git
git branch -M main
git push -uf origin main
```

## Интеграция с вашими инструментами (Integrate with your tools)

### Инструменты (Tools)
- Pip 24.0 
- Windows/Linux
- Python interpreter

### Библиотеки (Libraries)
```
import tkinter as tk 
from tkinter import messagebox, simpledialog, Menu, ttk
import sqlite3
from datetime import datetime
import openpyxl
from tkinter.filedialog import asksaveasfilename
```
### Среда разработки PyCharm (PyCharm development environment)

[JetBrains](https://www.jetbrains.com/ru-ru/pycharm/) - Pycharm



## Тестирование и развертывание (Test and Deploy)

Используйте встроенную непрерывную интеграцию в GitLab. (Use the built-in continuous integration in GitLab.)



***

# Editing this README

## Suggestions for a good README

## Name

## Description

## Badges

## Visuals

## Installation

## Usage

## Support

## Roadmap

## Contributing

## Authors and acknowledgment

## License


## Project status

