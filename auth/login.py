import tkinter as tk
from tkinter import messagebox
import sqlite3

def is_admin(c, username):
    # Проверяем, является ли указанный пользователь администратором
    c.execute('SELECT role FROM users WHERE username=?', (username,))
    role = c.fetchone()
    if role:
        return role[0] == "admin"
    else:
        return False


def login(root, c, conn, show_main_text, show_todo_app, employees_menu, menu, update_menu_text, username_entry,
          password_entry):
    def login_action():
        username = username_entry.get()
        password = password_entry.get()

        c.execute('SELECT * FROM users WHERE username=? AND password=?', (username, password))
        user = c.fetchone()
        if user:
            logged_in = True
            show_main_text()
            show_todo_app(username)
            employees_menu.entryconfig(0, state="normal")
            update_menu_text()
            if is_admin(c, username):
                menu.entryconfig(2, state="normal")
                menu.entryconfig(1, state="normal")

            # Directly display the main application window after successful login
            root.deiconify()  # Show the main application window
        else:
            messagebox.showerror("Error", "Incorrect username or password.")

    login_button = tk.Button(root, text="Login", command=login_action)
    login_button.pack()


def show_main_window(root):
    # Code to display the main application window goes here
    pass
