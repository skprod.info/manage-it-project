class TranslationManager:
    def __init__(self):
        self.translations = {
            "ru": {
                "menu_program": "Программа",
                "settings": "Настройки",
                "add_user": "Добавить пользователя",
                "export_report": "Выгрузка отчета",
                "exit": "Выход",
                "menu_employees": "Сотрудники",
                "view_list": "Просмотреть список",
                "delete_employee": "Удалить сотрудника",
                "menu_tasks": "Мои задачи",
                "archive": "Архив",
                "show_archive": "Показать архив",
                "ID": "Номер",
                "Task": "Задача",
                "Status": "Статус",
                "Date": "Дата",
                "Urgency": "Срочность",
                "Performer": "Исполнитель",
                "Error": "Ошибка",
                "user_id_error": "Не удалось получить ID текущего пользователя",
                "my_tasks": "Мои задачи",
                "select_language": "Выберите язык",
                "save": "Сохранить",
                "add_task": "Добавить задачу",
                "create_report": "Создать отчет",
                "login": "Войти",
                "register": "Регистрация",
                "login_error_message": "Неправильное имя пользователя или пароль.",
            },
            "en": {
                "menu_program": "Program",
                "settings": "Settings",
                "add_user": "Add User",
                "export_report": "Export Report",
                "exit": "Exit",
                "menu_employees": "Employees",
                "view_list": "View List",
                "delete_employee": "Delete Employee",
                "menu_tasks": "My Tasks",
                "archive": "Archive",
                "show_archive": "Show Archive",
                "ID": "ID",
                "Task": "Task",
                "Status": "Status",
                "Date": "Date",
                "Urgency": "Urgency",
                "Performer": "Performer",
                "Error": "Error",
                "user_id_error": "Failed to get current user ID",
                "my_tasks": "My Tasks",
                "select_language": "Select Language",
                "save": "Save",
                "add_task": "Add Task",
                "create_report": "Create Report",
                "login": "Login",
                "register": "Register",
                "login_error_message": "Incorrect username or password.",
            }
        }
        self.current_language = "ru"

    def set_language(self, lang):
        if lang in self.translations:
            self.current_language = lang

    def get_translation(self, key):
        return self.translations[self.current_language].get(key, key)